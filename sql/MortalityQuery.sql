SELECT
  CASE 
    WHEN [dec_sex] = '1' THEN 'M' 
    WHEN [dec_sex] = '2' THEN 'F' 
  END AS [sex], 
  'Death' AS [event_type], 
  CASE 
    WHEN [age_unit_code] > 1 THEN 0
    WHEN [age_at_death] > <max_age> THEN <max_age> 
    ELSE [age_at_death] 
  END AS [age], 
  LEFT([s_underlying_cod_icd10] + 'X', 4) AS [icd_10], 
  [lsoa_of_residence_code] AS [lsoa], 
  COUNT(*) AS [events] 
FROM
  [AnalystLocal].[Lancs].[Mortality_Data] 
WHERE LEFT([date_of_registration], 4) BETWEEN <start_year> AND <end_year>
GROUP BY
  [dec_sex], 
  [age], 
  [s_underlying_cod_icd10], 
  [lsoa_of_residence_code] 
