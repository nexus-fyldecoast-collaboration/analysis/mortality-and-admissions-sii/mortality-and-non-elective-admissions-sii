SELECT
  CASE 
	  WHEN [age] > <max_age> THEN <max_age> 
		ELSE [age] 
	END AS [age], 
	'Non-Elective Admission' AS [event_type], 
	LEFT([genderdescription], 1) AS [sex], 
	[lsoacode] AS [lsoa], 
	[primarydiagnosisicd10code] AS [icd_10], 
  COUNT(*) AS [events] 
FROM
  [SUS].[inpatientspell] 
WHERE
  [admissionmethodcode] IN('21', '22', '23', '24', '25', '28', '2A', '2B', '2D')
	AND [lsoacode] != '---------' 
	AND [dischargefinancialyearnumber] >= <start_year> 
	AND [dischargefinancialyearnumber] < <end_year> 
	AND LEFT([genderdescription], 1) IN ( 'M', 'F' ) 
GROUP BY 
  [age],
  [dischargefinancialyearnumber], 
	[genderdescription], 
	[lsoacode], 
	[gppracticecode], 
	[primarydiagnosisicd10code]
