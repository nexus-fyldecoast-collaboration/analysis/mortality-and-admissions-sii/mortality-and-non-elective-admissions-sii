# Mortality and Non Elective Admissions SII

In this project, we try to calculate the Slope Indexes of Inequality for either
Mortality data (life expectancy), or specific conditions (based on the ICD 10
code heirarchy).

## Configuration
File/folder paths and reference variables are set in `config.yml`. The reference
variables include:

* `start_year` = lower year boundary for SQL queries, to return non-elective
admissions data or mortality data
* `end_year` = upper year boundary for SQL queries, to return non-elective
admissions data or mortality data
* `max_age` = minimum age of patient, for age banding [years]
* `min_age` = maximum age of patient, for age banding [years]
* `age_band_width` = age band size [years]
* `n_tiles` = number of groups used to split IMD scores (e.g. 5 groups =
  quantiles)

The database details are stored in a separate config file, kept outside of the
git repository to keep user credentials from becoming visable to all. This is
referenced in `config.yml` via the variable `sql_credentials`, currently set as:

```
sql_credentials: !expr config::get(file = "H:/.config/sql.server.credentials.yml")
```

If moved, the path should be updated.

The DB credentials file should be set out as:

```
default:
  driver: "SQL Server"
  server: "MLCSU-BI-SQL.xlcsu.nhs.uk"
  database: "AnalystGlobal"
  port: "1433"
  uid: ...
  pwd: ...
```
where the user id and password can be stored in `uid` and `pwd` respectively.

## Required Packages
To run the project, the user must install four package groups:

```
* "config"
* "odbc"
* "readxl"
* "tidyverse"
```

This can be done via the command `install.packages(package_name)`.

## Running The Project
Once the config file has been set correctly, the project file `life_expectancy.
Rproj` should be opened; this is needed to make sure the file paths are correct.
The project can be run in R via:

```
source("~/life_expectancy/LifeExpectancy.R")
```
