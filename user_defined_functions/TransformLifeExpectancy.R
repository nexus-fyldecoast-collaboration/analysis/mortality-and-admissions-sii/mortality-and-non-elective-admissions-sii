### Create a function to create the table from which the SII and RII are calculated from
transform_life_expectancy <- function(le_df){
  filtered_le <- le_df %>% 
    select(sex, imd_group, denominator)
  
  le_df <- filtered_le %>%
    distinct() %>%
    arrange(sex, imd_group) %>%
    group_by(sex) %>%
    mutate(a = denominator / sum(denominator)) %>%
    mutate(
      b = (coalesce(lag(a), 0) / 2) + (a / 2),
      b = cumsum(b)
    )
  
  le_df %>%
    transmute(
      imd_group = imd_group,
      sqrt_a = sqrt(a),
      b_sqrt_a = sqrt_a * b
    )
}
