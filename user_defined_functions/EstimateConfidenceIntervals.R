
### Function to simulate confidence intervals for the slopes
estimate_confidence_intervals <- function(dsr_df) {
  dsr_df <- dsr_df %>%
    group_by(sex, icd_group) %>%
    mutate(
      prop_in_n_tile = Denominator / sum(Denominator, na.rm = TRUE),
      b1 = prop_in_n_tile / 2,
      b2 = coalesce(lag(b1), 0) + b1,
      b = coalesce(lag(b2), 0) + b2
    ) %>%
    ungroup()
  
  dsr_df %>%
    transmute(
      sex = sex,
      icd_group = icd_group,
      imd_group = imd_group,
      sqrt_proportion = sqrt(prop_in_n_tile),
      b_sqrt_prop = sqrt_proportion * b,
      value = `Std Rate (per 1000)` * sqrt_proportion
    )
}
