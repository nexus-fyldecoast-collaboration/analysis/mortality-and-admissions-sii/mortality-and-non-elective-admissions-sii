
### Calculate the slopes
calculate_slopes <- function(transformed_dsr_df){
  transformed_dsr_df <- transformed_dsr_df %>%
    group_by(sex, icd_group) %>%
    summarise(
      intercept = coef(lm(value ~ sqrt_proportion + b_sqrt_prop))[1],
      beta = coef(lm(value ~ sqrt_proportion + b_sqrt_prop))[2]
    ) %>%
    ungroup()
  
  transformed_dsr_df %>%
    transmute(
      sex = sex,
      icd_group = icd_group,
      SII = beta,
      RII = (intercept + beta) / (intercept + beta / 2)
    )
}
