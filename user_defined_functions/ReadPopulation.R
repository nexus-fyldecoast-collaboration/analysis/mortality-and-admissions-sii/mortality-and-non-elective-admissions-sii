# Read population file, select required gender data, filter to desired LSOAs,
#   and melt into long dataframe.
#
# Input: Gender ("Male" or "Female"); filter_df (dataframe to filter by area).
# Output: Dataframe with population split by LSOA, age, sex.

read_population <- function(gender, filter_df) {
  
  lancashire_population <- data.frame()
  years <- seq(config$start_year, config$end_year) %>%
    head(-1) %>%
    as.character()
  
  lancashire_population <- lapply(
    years,
    function(y) {
      cat('\t\tImporting', y, 'Population Data\n')
      
      # Don't have a population file for 2019 so just use 2018's and pray
      if (y == "2019") {
        y <- "2018"
      }
      
      # 2018 is saved in an .xlsx file, 2014-17 are saved in .xls files
      if (y == "2018") {
        x <- 'x'
      } else (
        x <- ''
      )
      
      # Read single-year file into dataframe and pivot to get ages/counts nicely
      one_year <- read_excel(
        paste0(config$first_population_data, y, config$last_population_data, x),
        sheet = paste0("Mid-", y, " ", gender),
        skip = 4
      ) %>%
        mutate(
          la = if_else(is.na(`Area Names`), NA_character_, `Area Codes`),
          lsoa = `Area Codes`
        ) %>%
        fill(la)
      
      la <- one_year %>%
        semi_join(filter(filter_df, type == 'la'), by = c("la" = "Code")) %>%
        select(-`All Ages`, -`Area Codes`, -`Area Names`, -`...3`, -la) %>%
        pivot_longer(cols = c("0":"90+")) %>%
        mutate(sex = str_sub(gender, 1, 1))
      
      lsoa <- one_year %>%
        semi_join(filter(filter_df, type == 'lsoa'), by = c("lsoa" = "Code")) %>%
        select(-`All Ages`, -`Area Codes`, -`Area Names`, -`...3`, -la) %>%
        pivot_longer(cols = c("0":"90+")) %>%
        mutate(sex = str_sub(gender, 1, 1))
      
      one_year <- bind_rows(la, lsoa)
      
      bind_rows(lancashire_population, one_year)
    }
  )
  
  cat('\n')
  
  return(lancashire_population)
}
