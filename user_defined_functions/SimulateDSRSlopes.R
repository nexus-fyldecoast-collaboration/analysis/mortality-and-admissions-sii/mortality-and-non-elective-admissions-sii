#
# A function to run N Monte Carlo simulations across M IMD groups and P ICD 
#   Categories. Linear regression is used to find the SII and RII for each ICD
#   category (in each Monte Carlo run), and results are aggregated to give final
#   means + 95% confidence intervals.
#
simulate_dsr_slopes <- function(dsr_df, dsr_transformed_df, crude_flag = FALSE) {
  # Select only useful columns from the DSR dataframe and split each ICD
  # category into a separate dataframe (joined in a list of DFs)
  if (crude_flag) {
    dsr_t <- dsr_df %>% 
      select(id, imd_group, crude_rate, crude_error, denominator)
  } else {
    dsr_t <- dsr_df %>% 
      select(id, imd_group, standard_rate, standard_error, denominator)
  }
  
  dsr_t <- split(dsr_t, dsr_t$id)
  
  # Find bad ICDs (which don't have valid DSRs for every IMD group) and remove
  bad <- NULL
  bad_id <- NULL
  
  remove_bads <- lapply(
    dsr_t, 
    function(d) {
      if (nrow(d) != config$n_tiles) {
        bad <<- c(bad, TRUE)
      } else {
        bad <<- c(bad, FALSE)
      }
    }
  )
  
  bad_id <- names(dsr_t)[bad]
  dsr_t <- dsr_t[!bad]
  
  # Create an NxMxP array to store Monte Carlo simulations in
  mc_n <- as.numeric(config$n_simulations)
  dsr_mc <- array(dim = c(config$n_tiles, mc_n, length(dsr_t)))
  
  # Generate MC distributions based upon observed DSRs and populate array
  for (i in 1:config$n_tiles) {
    for (j in seq_along(dsr_t)) {
      dsr_mc[i,, j] <- rnorm(
        mc_n,
        mean = as.numeric(dsr_t[[j]][i, 3]), 
        sd = as.numeric(dsr_t[[j]][i, 4])
      )
    }
  }
  
  # Transform MC DSRs to account for heteroskedacity
  dsr_mc <- dsr_mc * dsr_transformed_df$sqrt_a
  
  # Create empty beta-coefficient array and perform linear regression, using:
  #   beta = (X^T * X) * (X^T * y)
  # In beta array, first element is the intercept, second is the gradient
  beta <- array(dim = c(2, mc_n, length(dsr_t)))
  for (i in seq_along(dsr_t)) {
    X <- as.matrix(dsr_transformed_df[, 2:3])
    y <- as.matrix(dsr_mc[,,i])
    
    beta[,, i] <- solve(crossprod(X), crossprod(X, y))
  }
  
  # Create empty variables to store SII/RII means/confidence/p-values
  mean_sii <- mean_rii <- NULL
  upper_sii <- upper_rii <- NULL
  lower_sii <- lower_rii <- NULL
  p_val_sii <- p_val_rii <- NULL
  
  # Calculate SII/RII
  for (i in seq_along(dsr_t)) {
    mean_sii <- c(mean_sii, mean(beta[2, , i], na.rm = TRUE))
    upper_sii <- c(upper_sii, quantile(beta[2, , i], 0.975, na.rm = TRUE))
    lower_sii <- c(lower_sii, quantile(beta[2, , i], 0.025, na.rm = TRUE))
    sd_sii <- (upper_sii[i] - lower_sii[i]) / (2 * 1.96)
    z_sii <- abs(mean_sii[i] / sd_sii)
    p_val_sii <- c(p_val_sii, pmax(
      pmin(exp(-z_sii * (0.717 + z_sii * 0.416)), 1),
      0.0001
    ))
    
    rii <- beta[1,,i] / (beta[1,,i] + beta[2,,i])
    mean_rii <- c(mean_rii, mean(rii, na.rm = TRUE))
    upper_rii <- c(upper_rii, mean(rii, na.rm = TRUE) + (sd(rii, na.rm = TRUE) * 1.96))
    lower_rii <- c(lower_rii, mean(rii, na.rm = TRUE) - (sd(rii, na.rm = TRUE) * 1.96))
    
    sd_rii <- (log(abs(upper_rii[i])) - log(abs(lower_rii[i]))) / (2 * 1.96)
    
    z_rii <- abs(log(abs(mean_rii[i])) / sd_rii)
    p_val_rii <- c(p_val_rii, if_else(
      sign(upper_rii[i] / lower_rii[i]) == 1,
      pmax(
        pmin(exp(-z_rii * (0.717 + z_rii * 0.416)), 1),
        0.0001
      ),
      1
    ))
  }
  
  # Save important variables to a dataframe and return
  data.frame(
    id = names(dsr_t),
    sii = mean_sii,
    sii_lower = lower_sii,
    sii_upper = upper_sii,
    p_sii = p_val_sii,
    rii = mean_rii,
    rii_lower = lower_rii,
    rii_upper = upper_rii,
    p_rii = p_val_rii
  )
}
